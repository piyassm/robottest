*** Settings ***
Library     Selenium2Library
Library           OperatingSystem

*** Test Cases ***
Download PDF
    ${now}    Get Time    epoch
    ${download directory}    Join Path    ${OUTPUT DIR}    downloads_${now}
    Create Directory    ${download directory}

    ${chrome options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs}    Create Dictionary    download.default_directory=${download directory}
    Call Method    ${chrome options}    add_experimental_option    prefs    ${prefs}

    Create Webdriver    Chrome    chrome_options=${chrome options}
    Goto    https://www.adam.com.au/support/blank-test-files
    Wait Until Element Is Visible   //*[@id="main"]/div[3]/table/tbody/tr[1]/td[2]/a   30
    Click Element    //*[@id="main"]/div[3]/table/tbody/tr[1]/td[2]/a
    
    ${file}    Wait Until Keyword Succeeds    1 min    2 sec    Download should be done    ${download directory}
    Log to console    \n${file}
    Remove Directory    ${download directory}   recursive=True

*** Keywords ***
Download should be done
    [Arguments]    ${directory}
    ${files}    List Files In Directory    ${directory}
    Length Should Be    ${files}    1    Should be only one file in the download folder
    Should Not Match Regexp    ${files[0]}    (?i).*\\.crdownload    Chrome is still downloading a file
    ${file}    Join Path    ${directory}    ${files[0]}
    Log    File was successfully downloaded to ${file}
    [Return]    ${file}